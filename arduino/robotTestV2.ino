//#include <System.h>
#include <SPI.h>
#include <Ethernet.h>

int buzzerPin  = 5;
int motorR_Pin = 8;
int motorL_Pin = 9;
int calibrationTime = 20;   
// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
int lightPin = 0;
int thresholds[2] = { 
  400 , 800 };
int periodInSeconds = 10 ;

int const LOWLIGHT = 0;
int const MEDIUMLIGHT =1 ;
int const HIGHLIGHT = 2;


int lightPeriod = 0 ;

// Enter a MAC address for your controller below.
// Newer Ethernet shields have a MAC address printed on a sticker on the shield
byte mac[] = {  0x98, 0x4f, 0xEE, 0x01, 0x96, 0x95 };
IPAddress server(192,168,1,210); // Morad IP
EthernetClient client;


void setup()
{
  //system("ifdown eth0");
  //system("ifup eth0");
  
  Serial.begin (9600);
  Serial.println("Hello Plant!");

  //pinMode(buttonPin, INPUT);
  pinMode(buzzerPin, OUTPUT);
  pinMode(motorR_Pin, OUTPUT);
  pinMode(motorL_Pin, OUTPUT);
  digitalWrite(motorL_Pin, LOW) ;
  digitalWrite(motorR_Pin, LOW) ;

  Serial.println("Done Setup");
  delay(50);

  // start the Ethernet connection:
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // no point in carrying on, so do nothing forevermore:
    for(;;)
      ;
  }
  
  // give the Ethernet shield a second to initialize:
  delay(2000);
  Serial.println("connecting...");

}


int CurrentLightState() {
  int lightValue = ReadLight();
  if (lightValue < thresholds[0]) {
    Serial.println("LOW LIGHT ");
    return LOWLIGHT ;
  } 
  else if (lightValue > thresholds[1] ){
    Serial.println("HIGH LIGHT ");
    return HIGHLIGHT;
  } 
  else {
    Serial.println("MEDIUM LIGHT ");
    return MEDIUMLIGHT;
  }    
}

int ReadLight() {
  int reading  = analogRead(lightPin);
  delay(500);
  Serial.print("The volatge value:");
  Serial.println(reading,DEC);
  Serial.println("");
  return reading ;
}


void crazyMove()
{
  tone(buzzerPin,5000);
  digitalWrite(motorR_Pin, HIGH) ;
  digitalWrite(motorL_Pin, LOW) ;
  Serial.println("move");
  delay(1500);
  digitalWrite(motorR_Pin, LOW) ;
  digitalWrite(motorL_Pin, HIGH) ;
  delay(1500);
  digitalWrite(motorR_Pin, HIGH) ;
  digitalWrite(motorL_Pin, LOW);
  delay(1000);
  digitalWrite(motorR_Pin, LOW) ;
  digitalWrite(motorL_Pin, LOW);
  delay(100);
  noTone(buzzerPin);
}
void moveForw()
{
  digitalWrite(motorR_Pin, HIGH) ;
  digitalWrite(motorL_Pin, HIGH) ;
  Serial.println("move");
  delay(300);
  digitalWrite(motorR_Pin, LOW) ;
  digitalWrite(motorL_Pin, LOW);
  delay(100);
}
int getLightStatus()
{
  return 1;
}
void searchForLight()
{
  for(int n = 0 ; n < 12 ; n++)
  {
    delay(250);
    int plantStatus = getLightStatus();
    Serial.print("light status is:");
    Serial.println(plantStatus);

    if(plantStatus > 2)
    {
      return;
    }

    digitalWrite(motorL_Pin, LOW) ;
    digitalWrite(motorL_Pin, LOW) ;
    delay(100);
    digitalWrite(motorL_Pin, LOW) ;
  }
}

void reportStateToServer(int currentState){
  // if you get a connection, report back via serial:
  client.stop();
  delay(500);
  if (client.connect(server, 3000)) {
    Serial.println("connected");
    // Make a HTTP request:
	if(currentState == LOWLIGHT){
		client.println("GET /plantState?light_state=light_state0 HTTP/1.0");
	}else if(currentState == MEDIUMLIGHT){
	    client.println("GET /plantState?light_state=light_state1 HTTP/1.0");
	}else{
		client.println("GET /plantState?light_state=light_state2 HTTP/1.0");
	}
    client.println();
  } 
  else {
    // if you didn't get a connection to the server:
    Serial.println("connection failed");
  }
}

void loop()
{

  delay(1000);
  
  reportStateToServer(LOWLIGHT);

  while(true){
    delay(100);
    int currentState = CurrentLightState();
    
    Serial.print("light status");
    Serial.println(currentState);
    
    if (currentState == HIGHLIGHT) {
      reportStateToServer(HIGHLIGHT);
      Serial.println("I AM BURNING ");
      crazyMove();
    } else if (CurrentLightState() == MEDIUMLIGHT ) {
      reportStateToServer(MEDIUMLIGHT);
      Serial.println("MEDIUM");
    } else {
      reportStateToServer(LOWLIGHT);
      Serial.println("LOW");
    }
/*
    if(lightPeriod >= periodInSeconds){
      //start engine
      Serial.print("The Light Period [second]:");
      Serial.print(lightPeriod,DEC);
      Serial.println("Move 2 Meters away"); 
      lightPeriod = 0 ;     
    }*/
	
	//reportStateToServer(currentState);
  }
  /*delay(100);
   if(digitalRead(pirPin) == HIGH){
   Serial.println("pir ON");
   searchForLight();
   //crazyMove();
   //delay(1000);
   
   }else
   {
   Serial.println("pir OFF");
   }
   */
   
   //Update state to server
}
  //Read water state

  //Report state to server
  //string request = "http://323.3232.3.213.12/Sun={0}&Water={1};









