var express = require('express');
var router = express.Router();
var DB = require('../database/database')

var timeWhenReset;//Time in milliseconds at the last reset

/* GET home page. */
router.get('/', function(req, res) {
  var imageToUse;
  if(DB.states['light_state'] === 'light_state0'){
    imageToUse = 'noLight.jpg';
  } else if(DB.states['light_state'] === 'light_state1') {
    imageToUse = 'goodLight.jpg';
  } else if(DB.states['light_state'] === 'light_state2') {
    imageToUse = 'tooMuchLight.jpg';
  }

  var waterImageToUse;
  if(DB.states['water_state'] === 'water_state0'){
    waterImageToUse = 'waterLevel0.jpg';
  } else if(DB.states['water_state'] === 'water_state1') {
    waterImageToUse = 'waterLevel1.jpg';
  } else if(DB.states['water_state'] === 'water_state2') {
    waterImageToUse = 'waterLevel2.jpg';
  } else if(DB.states['water_state'] === 'water_state3') {
    waterImageToUse = 'waterLevel3.jpg';
  } else if(DB.states['water_state'] === 'water_state4') {
    waterImageToUse = 'thirsty.gif';
  }


  res.render('index', { title: 'Light State',
                        imageToUse: imageToUse,
                        waterImageToUse : waterImageToUse
                      }
            );
});

module.exports = router;
