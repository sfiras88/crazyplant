/**
 * Created by I300494 on 24/10/2014.
 */
var express = require('express');
var DB = require('../database/database')
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
    var light_state = req.query['light_state'];
    if(light_state === 'light_state0' || light_state === 'light_state1' || light_state === 'light_state2'){
        DB.states['light_state'] = light_state;
    }

    //TODO remove this to another place or handle it here...
    DB.states['humidity_state'] = req.query['humidity_state'];

    //The reply here is not needed...
    res.render('plantState', { state: 'FRESH!!!' });
});

module.exports = router;