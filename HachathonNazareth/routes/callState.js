/**
 * Created by I300494 on 24/10/2014.
 */
var express = require('express');
var DB = require('../database/database')
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
    var body = DB.states['call'];
    res.writeHead(200, {
        'Content-Length': body.length,
        'Content-Type': 'text/plain' });
    res.write(body);
});

module.exports = router;