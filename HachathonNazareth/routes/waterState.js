/**
 * Created by I300494 on 24/10/2014.
 */
var express = require('express');
var DB = require('../database/database')
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
    var water_state = req.query['water_state'];
    if(water_state === 'water_state0' ||
        water_state === 'water_state1' ||
        water_state === 'water_state2' ||
        water_state === 'water_state3'){
        DB.states['water_state'] = water_state;
        DB.states['call'] = 'false';
    }
    if(water_state === 'water_state4') {
        DB.states['water_state'] = water_state;
        DB.states['call'] = 'true';
    }

    res.render('waterState', { state: DB.states['water_state'] });
});

module.exports = router;